#! /usr/bin/python3

import os
import re
import shutil
import subprocess as sp
from time import sleep
# Reference for switch case implentation
# https://www.freecodecamp.org/news/python-switch-statement-switch-case-example/

# Reference for creating multiple Directory
# https://www.geeksforgeeks.org/make-multiple-directories-based-on-a-list-using-python/

# Reference for moving files to a Directory
# https://www.geeksforgeeks.org/how-to-move-all-files-from-one-directory-to-another-using-python/


# VARIABLES
home = os.path.expanduser('~')
FONT_DIR = os.path.expanduser('/usr/share/fonts')
THEMES_DIR = os.path.expanduser('/usr/share/themes')
ICONS_DIR = os.path.expanduser('/usr/share/icons')

source = home + "/fedora-minimal/files/dotconfig"
source_folder = home + "/fedora-minimal/files/Images"
destination = home + "/.config"
destination_folder = home + "/Wallpapers"


choice = ""
options = ""
yes_choices = ['yes', 'y']
no_choices = ['no', 'n']


# USER VARS
ins_eine = "dnf"
ins_zwei = "apt-get"
ins_drei = "pacman"
ins_vier = "zypper"
# ins_fünf= ""

# Colors


def prRed(skk): print("\033[91m {}\033[00m" .format(skk))


def prRedHighlight(skk):
    print("\033[91m \033[4m \033[7m {}\033[00m" .format(skk))


def prGreen(skk): print("\033[32m {}\033[00m" .format(skk))


def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))


def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))


def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))


def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))


def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))


def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))


def prBrightPurple(skk): print("\033[35m {}\033[00m" .format(skk))


def prboldRed(skk): print("\033[31m {}\033[00m" .format(skk))


def primary_menu():
    prCyan("*** Welcome to BSPWM Install ***")
    prCyan("*** Please Choose your Operation System ***")
    prCyan("*** Only four supported as of this moment ***")
    prCyan("*** 1. Fedora Linux ***")
    prCyan("*** 2. Debian Linux (Sid) ***")
    prCyan("*** 3. Arch Linux ***")
    prCyan("*** 4. Opensuse Tumbleweed ***")
    prCyan("*** 5. Configure the Dotfiles, Create directory, fonts, icons ***")
    prCyan("*** 6. User Configuration ***")
    prCyan("*** 7. Set Lightdm Settings ***")
    prCyan("*** 8. Completed Installer & Exit ***")


def fedora_minimal():
    prboldRed("Basic Software and Update the system")
    sp.call(["sudo", ins_eine, "update"])
    sp.call(["sudo", ins_eine, "install",
             "glx-utils", "mesa-dri-drivers",
             "mesa-vulkan-drivers", "plymouth-system-theme",
             "xorg-x11-drv-evdev", "xorg-x11-drv-fbdev"])
    sp.call(["sudo", ins_eine, "install", "xorg-x11-drv-intel",
             "xorg-x11-drv-libinput", "xorg-x11-drv-vesa", "xorg-x11-xauth",
             "xorg-x11-xinit", "xorg-x11-server-Xorg",
             "xbacklight", "redshift"])


def fedora_essential():
    print("Install Basic Software , essential software and update")
    sleep(3)
    sp.call(["sudo", ins_eine, "install", "bspwm", "sxhkd", "kitty"
             "rofi", "picom", "thunar", "nitrogen",
             "mate-polkit", "gnome-keyring", "brightnessctl",
             "playerctl", "xsel", "xclip", "arandr",
             "xkill", "xsetroot", "tree", "feh",
             "unzip", "flameshot", "fzf", "cava",
             "calcurse", "ranger", "lightdm", "lightdm-gtk-greeter",
             "lightdm-settings", "lightdm-gtk-greeter-settings",
             "light-locker", "notification-daemon", "xdotool",
             "libappindicator", "linux-firmware", "acpi",
             "cairo", "glib2", "stalonetray"])
    print("Install Other Software")
    sp.call(["sudo", ins_eine, "install", "mangohud"])
    sp.call(["sudo", ins_eine, "install", "gimp", "golang", "vim",
             "neovim", "neofetch", "lxappearance", "aria2", "zathura",
             "zathura-pdf-poppler", "gparted", "gnome-disk-utility",
             "udiskie", "upower", "xdg-utils", "xdg-user-dirs",
             "xdg-user-dirs-gtk", "xdg-desktop-portal",
             "dbus-x11", "dnf-utils", "pesign", "keepassxc",
             "rclone", "rsync", "galculator", "viewnior"])
    print("Installing Other Programs Printing support")
    sp.call(["sudo", ins_eine, "install", "cups", "hplip"])
    print("Installing Custom ocs-url package")
    sp.call(["sudo", ins_eine, "install", home +
            "/rpm-packages/ocs-url-3.1.0-1.fc20.x86_64.rpm"])
    print("Installing Network tools")
    sp.call(["sudo", ins_eine, "install", "NetworkManager-tui",
             "NetworkManager-wifi", "network-manager-applet",
             "wireless-tools", "net-tools",
             "nmap", "traceroute"])
    print("Installing Sound Software")
    sp.call(["sudo", ins_eine, "install", "gnome-sound-recorder",
             "pulseaudio", "pipewire",
             "pipewire-doc",  "pipewire-gstreamer",  "pipewire-alsa",
             "alsa-utils", "wireplumber",
             "gstreamer1", "gstreamer1-plugins-bad-free",
             "gstreamer1-plugins-bad-free-gtk", "gstreamer1-plugins-base",
             "gstreamer1-plugins-good", "pavucontrol",
             "gstreamer1-plugin-openh264"])
    print("Installing fonts")
    sp.call(["sudo", ins_eine, "install", "fontawesome-fonts",
             "fontawesome-fonts-web", "dejavu-sans-fonts",
             "dejavu-sans-mono-fonts", "dejavu-serif-fonts",
             "google-droid-fonts",
             "adobe-source-han-sans-cn-fonts",
             "adobe-source-han-serif-cn-fonts",
             "liberation-mono-fonts", "liberation-sans-fonts",
             "liberation-serif-fonts"])
    print("Installing Themes")
    sp.call(["sudo", ins_eine, "install", "adwaita-gtk2-theme",
             "adwaita-icon-theme",
             "papirus-icon-theme",
             "materia-gtk-theme"])
    print("Installing Software to build Cursors")
    sp.call(["sudo", ins_eine, "install", "libX11-devel", "libXcursor-devel",
             "libpng-devel", "libXtst-devel"])
    prPurple("Installing Color picker")
    sp.call(["sudo", ins_eine, "install", "gpick"])
    prPurple("Installing Screenkey")
    sp.call(["sudo", ins_eine, "install", "screenkey"])


def fedora_everything():
    print("*** Full Installation of Essential Software,Update your System ***")
    sleep(3)
    fedora_minimal()
    fedora_essential()


def arch_minimal():
    print("*** Update the System and Install Xorg dependency ***")
    sp.call(["sudo", ins_drei, "-Syyy"])
    sp.call(["sudo", ins_drei, "-Syu"])
    sp.call(["sudo", ins_drei, "-S", "xorg", "xorg-server",
             "xorg-xinit", "xdo", "xdotool",
             "xorg-xkill", "xorg-server-common", "xorg-server-devel",
             "xorg-xsetroot", "xorg-server-xephyr", "dbus",
             "xorg-server-xnest", "xorg-server-xvfb"])


def arch_essential():
    print("Install Essential Software")
    sleep(3)
    sp.call(["sudo", ins_drei, "-S", "bspwm", "sxhkd", "picom",
             "kitty", "rofi", "thunar", "arandr",
             "nitrogen", "mate-polkit", "upower",
             "gnome-keyring", "brightnessctl",
             "playerctl", "xsel", "xclip",
             "xorg-xkill", "tree", "feh",
             "zip", "unzip",
             "flameshot", "fzf",
             "calcurse", "ranger",
             "lightdm", "lightdm-gtk-greeter", "accountsservice",
             "lightdm-gtk-greeter-settings", "light-locker",
             "notification-daemon", "libappindicator-gtk3",
             "acpi", "cairo",
             "glib2", "stalonetray"])
    print("Installing Other Programs")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "gimp", "go", "vim", "neovim",
             "lxappearance", "aria2", "zathura", "zathura-pdf-mupdf",
             "zathura-djvu", "gparted", "gnome-disk-utility",
             "udiskie", "upower", "neofetch", "gvfs", "gvfs-nfs",
             "xdg-utils", "xdg-user-dirs",
             "xdg-user-dirs-gtk", "xdg-desktop-portal",
             "keepassxc", "rsync",
             "rclone", "galculator", "viewnior"])
    print("Installing Other Programs Printing support")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "cups", "hplip"])
    print("Installing Other Programs Openssh ")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "openssh",
             "x11-ssh-askpass", "xorg-xauth"])
    print("Installing Network Tools")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "networkmanager",
             "network-manager-applet", "net-tools",
             "nm-connection-editor", "traceroute",
             "networkmanager-openvpn", "networkmanager-vpnc", "netstat-nat"])
    print("Installing Sound Support Software")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "gnome-sound-recorder",
             "pavucontrol", "pulseaudio",
             "pipewire", "pipewire-alsa"])
    print("Installing Fonts Support")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "ttf-font-awesome", "ttf-dejavu",
             "adobe-source-han-sans-cn-fonts",
             "adobe-source-han-serif-cn-fonts",
             "ttf-liberation", "noto-fonts", "gnu-free-fonts",
             "ttf-droid"])
    print("Installing GTK THEMES")
    sleep(5)
    sp.call(["sudo", ins_drei, "-S", "papirus-icon-theme",
             "materia-gtk-theme"])
    prPurple("Installing Color picker")
    sp.call(["sudo", ins_drei, "-S", "gpick"])
    prPurple("Installing Screenkey")
    sp.call(["sudo", ins_drei, "-S", "screenkey"])


def arch_everything():
    print("*** Full Installation of Essential Software,Update your System ***")
    sleep(3)
    arch_minimal()
    arch_essential()


def debian_minimal():
    print("*** Update the System ***")
    sp.call(["sudo", ins_zwei, "update", "&&", "sudo", ins_zwei, "upgrade"])


def debian_essential():
    print("Install Essential Software")
    sleep(3)
    sp.call(["sudo", ins_zwei, "install", "bspwm", "sxhkd", "kitty",
             "rofi", "picom", "thunar", "nitrogen", "mate-polkit",
             "gnome-keyring", "brightnessctl", "brightnessctl", "xsel",
             "xclip", "x11-utils", " x11-xserver-utils",
             "tree", "feh", "unzip", "arandr",
             "flameshot", "fzf", "calcurse", "cava",
             "ranger", "lightdm", "lightdm-gtk-greeter",
             "lightdm-gtk-greeter-settings", "light-locker",
             "lightdm-settings", "notification-daemon",
             "acpi", "acpi-call-dkms", "stalonetray"])
    print("Installing Other Programs")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "mangohud"])
    print("Installing Other Programs")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "gimp", "golang",
             "vim", "neovim", "neofetch", "lxappearance", "aria2",
             "zathura", "zathura-djvu", "zathura-pdf-poppler",
             "gparted", "gnome-disk-utility", "udiskie",
             "upower", "xdg-utils", "xdg-user-dirs", "xdg-user-dirs-gtk",
             "xdg-desktop-portal", "dbus-x11", "keepassxc",
             "rsync", "rclone", "galculator", "viewnior",
             ])
    print("Installing Printing Software")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "cups", "hplip"])

    print("Installing Openssh Software")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "openssh-client", "openssh-server"])
    print("Installing Network Tools")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "network-manager",
             "nmap", "traceroute"])
    print("Installing Sound Software ")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "pavucontrol", "playerctl",
             "pipewire", "sound-theme-freedesktop", "alsa-utils", "pulseaudio",
             "gnome-sound-recorder"])
    print("Installing Fonts")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "ttf-mscorefonts-installer"])
    sp.call(["sudo", ins_zwei, "install", "fonts-dejavu-core",
             "fonts-font-awesome",
             "fonts-liberation", "fonts-terminus",
             "fonts-droid-fallback", "fonts-noto-core",
             "fonts-noto-mono", "fonts-ubuntu", "ttf-ubuntu-font-family"])
    print("*** Installing  Python Support ***")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "python3", "python-is-python3",
             "python-pip", "python3-xlib",
             "python3-wheel", "python3-apt",
             "python3-cairo", "python3-dbus",
             "python3-dev", "python3-minimal",
             "python3-distutils", "python3-debian"])
    print("Installing GTK THEMES")
    sleep(5)
    sp.call(["sudo", ins_zwei, "install", "materia-gtk-theme",
             "papirus-icon-theme"])
    prPurple("Installing Color picker")
    sp.call(["sudo", ins_zwei, "install", "gpick"])
    prPurple("Installing Screenkey")
    sp.call(["sudo", ins_zwei, "install", "screenkey"])


def debian_everything():
    print("*** Full Installation of Essential Software,Update your System ***")
    sleep(3)
    debian_minimal()
    debian_essential()


def opensuse_minimal():
    print("*** Installing Xorg Refresh / Update the System ***")
    prRedHighlight("*** Adding repository for Xorg  ***")
    prRedHighlight("*** Skip if you add during the installation process ***")
    while True:
        user_input = input("Adding repository \nType yes or no:\n")
        if user_input.lower() in yes_choices:
            sp.call(["sudo", ins_vier, "addrepo",
                     "https://download.opensuse.org/repositories/X11:XOrg/openSUSE_Tumbleweed/X11:XOrg.repo"])
            sp.call(["sudo", ins_vier, "addrepo",
                     "http://download.opensuse.org/tumbleweed/repo/oss/"])
            sp.call(["sudo", ins_vier, "addrepo",
                    "https://download.opensuse.org/repositories/openSUSE:Factory/standard/openSUSE:Factory"])
        elif user_input.lower() in no_choices:
            print("Refresh and Upgrade the System")
            sp.call(["sudo", ins_vier, "refresh"])
            sp.call(["sudo", ins_vier, "dist-upgrade"])
            sp.call(["sudo", ins_vier, "install",
                     "xorg-x11", "xorg-x11-server",
                     "xorg-x11-util-devel", "xinit", "xdotool",
                     "xorg-x11-server-extra", "dbus-1",
                     "xsetroot", "xdo", "xkill"])
            sp.call(["sudo", ins_vier, "install",
                     "xdg-user-dirs", "xdg-desktop-portal",
                     "xdg-user-dirs-gtk"])
            break
        else:
            print("Type yes or no")
            continue


def opensuse_essential():
    print("Install Essential Software")
    sp.call(["sudo", ins_vier, "install", "bspwm", "sxhkd", "kitty",
             "rofi", "thunar", "picom", "nitrogen", "arandr",
             "mate-polkit", "gnome-keyring", "brightnessctl", "playerctl",
             "unzip", "feh", "tree", "flameshot", "psmisc",
             "xset", "xsettingsd", "xsetpointer", "xsetmode",
             "xclip", "xsel", "cava", "libnotify-tools",
             "xkill", "fzf", "calcurse", "ranger",
             "lightdm", "lightdm-gtk-greeter", "lightdm-gtk-greeter-settings",
             "light-locker", "notification-daemon",
             "acpi", "acpid",
             "stalonetray"])
    print("Installing Other Software")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "mangohud"])
    print("Installing Other Software")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "gimp", "golang-org-x-sys",
             "golang-packaging", "vim", "neovim", "neofetch",
             "lxappearance", "aria2", "libXss-devel", "xscreensaver",
             "zathura", "zathura-plugin-djvu",
             "zathura-plugin-pdf-mupdf",
             "upower", "udiskie",
             "xdg-user-dirs", "xdg-user-dirs-gtk",
             "xdg-desktop-portal", "xdg-desktop-portal-gtk"
             "keepassxc", "dbus-1-x11",
             "gparted", "gnome-disk-utility",
             "rsync", "rclone"
             "galculator",
             "viewnior"])
    print("Installing Other Programs Openssh ")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "openssh", "openssh-server",
             "openssh-clients"])
    print("Installing Network Tools")
    sleep(5)
    sp.call(["sudo", ins_vier, "install",
             "nmap", "traceroute"])
    print("Installing Sound Software ")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "pavucontrol", "playerctl",
             "pipewire", "pipewire-pulseaudio",
             "sound-theme-freedesktop", "alsa-utils",
             "pulseaudio",
             "gnome-sound-recorder"])
    print("Installing Printing Software")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "cups", "hplip"])
    print("Installing Fonts Support")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "google-droid-fonts", "dejavu-fonts",
             "noto-sans-fonts", "gnu-free-fonts",
             "liberation-fonts",
             "adobe-sourcehansans-fonts", "adobe-sourcehanserif-fonts",
             "fontawesome-fonts"])
    print("Installing GTK THEMES")
    sleep(5)
    sp.call(["sudo", ins_vier, "install", "materia-gtk-theme",
             "papirus-icon-theme"])
    prPurple("Installing Color picker")
    sp.call(["sudo", ins_vier, "install", "gpick"])
    prPurple("Installing Screenkey")
    sp.call(["sudo", ins_vier, "install", "screenkey"])


def opensuse_everything():
    print("*** Full Installation of Essential Software,Update your System ***")
    sleep(5)
    opensuse_minimal()
    opensuse_essential()


def DisplayCompleted():
    prGreen("You have completed the Installer ")
    prGreen("Please check your system to ensure everything is installed")
    prGreen("Read the ***README*** file in the folder for more information.")


def DotConfigSettings():
    print("Creating dotconfig folder and other essential folder")
    os.makedirs(home + "/Wallpapers", exist_ok=True)
    os.makedirs(home + "/.config", exist_ok=True)
    os.makedirs(home + "/Screenshot-Ordner", exist_ok=True)

    while True:
        user_input = input("Create optional folders\nType yes or no:\n")
        if user_input.lower() in yes_choices:
            print("Creating Optionally Folders")
            os.makedirs(home + "/Arbeitsverzeichnis", exist_ok=True)
            os.makedirs(home + "/Test", exist_ok=True)
            break
        elif user_input.lower() in no_choices:
            print("Not Creating Optional Folders")
            break
        else:
            print("Type yes or no")
            continue

    users_input = input("Create Optionally Folder:AURHELPER\nType yes or no: ")
    if users_input.lower() == 'yes':
        print("Creating Folder")
        os.makedirs(home + "/AURHELPER", exist_ok=True)
    elif users_input.lower() == 'no':
        print("Nothing to do")
    else:
        print("Type yes or no")

    decision_input = input(
        "Moved Essential folder to their respective folder\nType yes or no: ")
    if decision_input.lower() == 'yes':

        #    fetch all files
        allfiles = os.listdir(source)
        primaryfiles = os.listdir(source_folder)
        print("Files to move", allfiles)
        print("Files to move", primaryfiles)
        #   Choose a  destination folder to relocate all files.
        for file_path in allfiles:
            src_path = os.path.join(source, file_path)
            dst_path = os.path.join(destination, file_path)
            shutil.move(src_path, dst_path)
            print(f"Moved {file_path} -> {dst_path}")
            sleep(10)

        for file_primary in primaryfiles:
            src_path = os.path.join(source_folder, file_primary)
            dst_path = os.path.join(destination_folder, file_primary)
            shutil.move(src_path, dst_path)
            print(f"Moved {file_primary} -> {dst_path}")
            sleep(10)

    elif decision_input.lower() == 'no':
        print("Nothing to do")
    else:
        print("Type yes or no")


def GetDownloads():
    print("Installing Fonts , Themes , Cursors Themes ")
    sleep(5)
    sp.call(["aria2c", "-x", "4", "-s", "4",
             "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FantasqueSansMono.zip"])
    sp.call(["aria2c", "-x", "4", "-s", "4",
             "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip"])
    sp.call(["aria2c", "-x", "4", "-s", "4",
             "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip"])
    sp.call(["aria2c", "-Z",
             "https://www.1001fonts.com/download/alte-schwabacher.zip",
             "https://www.1001fonts.com/download/breitkopf-fraktur.zip",
             "https://www.1001fonts.com/download/barloesius-schrift.zip",
             "https://www.1001fonts.com/download/strassburg-fraktur.zip",
             "https://www.1001fonts.com/download/allura.zip",
             "https://www.1001fonts.com/download/shannia.zip"])
    sp.call(["aria2c", "-x", "4", "-s", "4",
             "https://github.com/dracula/gtk/archive/master.zip"])
    sp.call(["aria2c", "-Z",
             "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/JetBrainsMono.zip",
             "https://github.com/tabler/tabler-icons/releases/download/v1.119.0/tabler-icons-1.119.0.zip",
             "https://github.com/rsms/inter/releases/download/v3.19/Inter-3.19.zip"])


def SetDownloads():
    print("Configure Fonts, Cursors Themes, Themes")
    sleep(5)
    sp.call(["sudo", "unzip", home + "/fedora-minimal/FiraCode.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/FantasqueSansMono.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/Meslo.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/alte-schwabacher.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + 'zip.+',
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/tabler-icons-1.119.0.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/gtk-master.zip",
             "-d", THEMES_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/JetBrainsMono.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "unzip", home + "/fedora-minimal/Inter-3.19.zip",
             "-d", FONT_DIR])
    sp.call(["sudo", "fc-cache", "-vf"])


def SetLightdm():
    print("Configure Lightdm Settings ")
    sleep(5)
    sp.call(["sudo", "/usr/bin/bash",
             home + "/fedora-minimal/files/user_settings.sh"])


def UserConfig():
    print("Installing Fonts for Rofi")
    sleep(5)
    sp.call(["/usr/bin/bash", home + "/fedora-minimal/files/setup_fonts.sh"])


def secondary_menu():
    prGreen("Please Choose a valuable option")
    prGreen("1.  fedora-minimal")
    prGreen("2.  fedora-essential")
    prGreen("3.  fedora-everything")
    prGreen("4.  debian-minimal")
    prGreen("5.  debian-essential")
    prGreen("6.  debian-everything")
    prGreen("7.  arch-minimal")
    prGreen("8.  arch-essential")
    prGreen("9.  arch-everything")
    prGreen("10. opensuse-minimal")
    prGreen("11. opensuse-essential")
    prGreen("12. opensuse-everything")
    prGreen("13. GetDownloads")
    prGreen("14. SetDownloads")
    prGreen("15. Return to main menu")
    prGreen("20. Completed Installer & Exit")


def bspwm_colors():
    prPurple(
        "╔══╗ ╔═══╗╔═══╗╔╗╔╗╔╗╔═╗╔═╗    ╔══╗╔═╗ ╔╗╔═══╗╔════╗╔═══╗╔╗   ╔╗")
    prPurple(
        "║╔╗║ ║╔═╗║║╔═╗║║║║║║║║║╚╝║║    ╚╣╠╝║║╚╗║║║╔═╗║║╔╗╔╗║║╔═╗║║║   ║║")
    prPurple(
        "║╚╝╚╗║╚══╗║╚═╝║║║║║║║║╔╗╔╗║     ║║ ║╔╗╚╝║║╚══╗╚╝║║╚╝║║ ║║║║   ║║")
    prPurple(
        "║╔═╗║╚══╗║║╔══╝║╚╝╚╝║║║║║║║     ║║ ║║╚╗║║╚══╗║  ║║  ║╚═╝║║║ ╔╗║║ ╔╗")
    prPurple(
        "║╚═╝║║╚═╝║║║   ╚╗╔╗╔╝║║║║║║    ╔╣╠╗║║ ║║║║╚═╝║ ╔╝╚╗ ║╔═╗║║╚═╝║║╚═╝║")
    prPurple(
        "╚═══╝╚═══╝╚╝    ╚╝╚╝ ╚╝╚╝╚╝    ╚══╝╚╝ ╚═╝╚═══╝ ╚══╝ ╚╝ ╚╝╚═══╝╚═══╝")

    print("\n")


bspwm_colors()


while True:
    primary_menu()
    try:
        choice = int(input("Please choose a number : 1,2,3,4,5, 6, 7, 8:\n"))
    except NameError:
        prRedHighlight("Wrong input. Please enter a number between 1-8 ...")
    except:
        print("Wrong input. Please enter a number between 1-8 ...")

    match choice:
        case 1:
            print("Fedora Linux")
            sleep(3)
        case 2:
            print("Debian Linux (Sid)")
            sleep(3)
        case 3:
            print("Arch Linux")
            sleep(3)
        case 4:
            print("Opensuse Tumbleweed")
            sleep(3)
        case 5:
            DotConfigSettings()
        case 6:
            UserConfig()
        case 7:
            SetLightdm()
        case 8:
            print("Exit the code ...")
            DisplayCompleted()
            exit()
        case _:
            print("Invalid option detected!!!")
    os.system('clear')
    secondary_menu()
    try:
        options = int(
            input("Please choose a number between  : 1-15, 20:\n"))
    except:
        prRedHighlight("Please enter a number between 1-15 20(exit) ...")

    match options:
        case 1:
            fedora_minimal()
        case 2:
            fedora_essential()
        case 3:
            fedora_everything()
        case 4:
            debian_minimal()
        case 5:
            debian_essential()
        case 6:
            debian_everything()
        case 7:
            arch_minimal()
        case 8:
            arch_essential()
        case 9:
            arch_everything()
        case 10:
            opensuse_minimal()
        case 11:
            opensuse_essential()
        case 12:
            opensuse_everything()
        case 13:
            GetDownloads()
        case 14:
            SetDownloads()
        case 15:
            prRedHighlight("Returning to main menu")
            sleep(3)
            primary_menu()
            os.system('clear')
        case 20:
            print("Exit the code ...")
            DisplayCompleted()
            exit()
        case _:
            print("Invalid option detected !!!")
            continue
