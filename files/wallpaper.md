## Wallpapers

**Notice:**
All wallpappers used in this respostory are not own by me. This will be used as a reference for most of the images.
Images were mostly taken from [wallhaven](https://wallhaven.cc) or [wallpapercave](https://wallpapercave.com/) website.

- https://wallhaven.cc/w/nkxr6q

- https://wallhaven.cc/w/zyxvqy

- https://wallhaven.cc/w/o3m6ep

- https://wallhaven.cc/w/vq9eyl

- https://wallhaven.cc/w/j5pr65

- https://wallhaven.cc/w/43edrd

Neon Shallows wallpapers:

- https://wallpapercave.com/neon-shallows-wallpapers

- https://wallhaven.cc/w/136m9w \(Original Source\)

- https://raw.githubusercontent.com/justleoo/dotfiles/sway/conf/sway/wallpaper.png
