
# Fish Shell 

Install the necessary dependencies. 

**Dependency**

- [exa](https://the.exa.website/)

- [starship prompt](https://starship.rs/)

- [fish shell](https://fishshell.com/)

## Wezterm terminal

The wezterm used by default the fish shell , if you want to configure your terminal refer to the wezterm website: 

- https://wezfurlong.org/wezterm/index.html


