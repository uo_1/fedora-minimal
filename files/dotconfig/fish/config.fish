if status is-interactive
    # Commands to run in interactive sessions can go here
end

if [ -x "$(command -v exa)" ];
    alias ls="exa"
    alias la="exa --long --all --group"
    alias lT="exa --icons --tree"
end



alias bat='acpi -V'
alias dfa='sudo btrfs filesystem usage /'
alias  btrfsl='sudo btrfs subvolume list -a /'
alias  btrfsls='sudo btrfs subvolume list -s /'

set fish_greeting

starship init fish | source
