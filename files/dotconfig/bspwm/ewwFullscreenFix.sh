#!/bin/bash

bspc subscribe node_state | while read -r _ _ _ _ state flag; do
	if [ "$state" != "fullscreen" ]; then
		continue
	fi
	if [ "$flag" == on ]; then
		xdo lower -N eww-bar
		#killall -q stalonetray
		pkill stalonetray

	else
		xdo raise -N eww-bar
		## Use to solve system tray disappear after bar returns 
		## Restart bspwm if system tray doesn't appear 
		killall -q stalonetray; stalonetray & 
	fi
done
