-- # Personal Preferences

require('rin.personal.indent')
require('rin.personal.line')
require('rin.personal.window')
require('rin.personal.tab').setup()
require('rin.personal.mouse')
