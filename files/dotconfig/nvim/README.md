# Detail

The entire configuration using lua, don't forget to check every plugin documentation for further information, I think each files is pretty self-explanatory.

# Dependency

- packer : https://github.com/wbthomason/packer.nvim

# Tips 

How can I install plugins automatically with packer.vim ? 

The solution I found on reddit to update or install plugins with packer. 

Use the command below to install or update: 

	`nvim +PackerInstall`  
		**OR**
	`nvim +PackerUpdate`

- Source : https://www.reddit.com/r/neovim/comments/ri0vpv/how_can_i_install_plugins_automatically_with/
