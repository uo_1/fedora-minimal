## **Vim Dependencies**

---

`Lightline`

\# Reference : https://github.com/itchyny/lightline.vim

## **Configure**

---

- Move the vimrc file

cd ~/.config/vim/

mv -v .vimrc ~/
