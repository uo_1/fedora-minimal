## **Dependency**

---

- Any nerd font should work. (Icons)

- Font: [FiraCode Nerd Font Mono](https://www.nerdfonts.com/)

## **Configuration**

- Read the neofetch wiki on how to configure.

- Reference: https://github.com/dylanaraps/neofetch/wiki/Customizing-Info

The Documentation is self explanatory, which helps with configuring neofetch and any
other information related to the software.

## **Neofetch**

Directly from [neofetch website](https://github.com/dylanaraps/neofetch):

```

Neofetch is a command-line system information tool written in bash 3.2+.

Neofetch displays information about your operating system,

software and hardware in an aesthetic and visually pleasing way.


```
