**Dependency**

- [wpgtk](https://github.com/deviantfero/wpgtk)

- [pywal](https://github.com/dylanaraps/pywal)

Ensure you have the necessary dependency.Lets begin .
Create the folder below if it doesn't already exist.

```

  mkdir -p /usr/share/applications
  cd ~/fedora-minimal/files/dotconfig/wpg
  cp -r  -v  wpgtk.desktop       /usr/share/application/
```
