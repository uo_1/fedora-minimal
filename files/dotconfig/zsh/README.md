## **Configure Zsh Shell**

---

Follow the instructions from the link below to configure zsh.

Using oh-my-zsh , zsh shell and etc.

- https://dev.to/abdfnx/oh-my-zsh-powerlevel10k-cool-terminal-1no0

- https://the.exa.website/

Add the following to your `.zshrc` file:

Use your Editor of choice:

vim ~/.zshrc

```
# Path to rofi scripts
export PATH=$HOME/.config/rofi/scripts:$PATH

```
