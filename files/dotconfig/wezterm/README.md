# Fish Shell

Install the necessary dependencies.

**Dependency**

- [exa](https://the.exa.website/)

- [starship prompt](https://starship.rs/)

- [fish shell](https://fishshell.com/)

## Wezterm terminal

The wezterm used by default the fish shell , if you want to configure your terminal refer to the wezterm website:

- https://wezfurlong.org/wezterm/index.html

If you are using another shell from the fish shell and you want to use the samething in the wezterm terminal.
Comment out the section in the wezterm.lua file.

Reference : https://wezfurlong.org/wezterm/config/launch.html

```
    -- Spawn a fish shell in login mode
  default_prog = { '/usr/local/bin/fish', '-l' },

```
