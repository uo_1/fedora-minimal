-- Instruction to configure can be found on the wezterm website
-- https://wezfurlong.org/wezterm/config/files.html

local wezterm = require 'wezterm'
local act = wezterm.action


return {

  -- Start a fish shell in login mode
  default_prog = { '/usr/bin/fish', '-l' },

  --Font
  font = wezterm.font 'Fira Code',
  harfbuzz_features = {"zero" , "ss01", "cv05"},
  -- You can specify some parameters to influence the font selection;
  -- for example, this selects a Bold, Italic font variant.
  font = wezterm.font('JetBrains Mono', { weight = 'Bold', italic = true }),
  font_size = 16.0,
  line_height = 0.95,

  --Colors
  --color_scheme = 'CloneofUbuntu (Gogh)',
  color_scheme = "tokyonight",
  --color_scheme = "Treehouse",


  -- Retro Tab bar
  -- Colors
  colors = {
    tab_bar = {
      -- The color of the strip that goes along the top of the window
      -- (does not apply when fancy tab bar is in use)
      background = '#171f33',

      -- The active tab is the one that has focus in the window
      active_tab = {
        -- The color of the background area for the tab
        bg_color = '#7aa2f7',
        -- The color of the text for the tab
        fg_color = '#1f2335',

        -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
        -- label shown for this tab.
        -- The default is "Normal"
        intensity = 'Normal',

        -- Specify whether you want "None", "Single" or "Double" underline for
        -- label shown for this tab.
        -- The default is "None"
        underline = 'None',

        -- Specify whether you want the text to be italic (true) or not (false)
        -- for this tab.  The default is false.
        italic = false,

        -- Specify whether you want the text to be rendered with strikethrough (true)
        -- or not for this tab.  The default is false.
        strikethrough = false,
      },

      -- Inactive tabs are the tabs that do not have focus
      inactive_tab = {
        bg_color = '#292e42',
        fg_color = '#545c7e',

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `inactive_tab`.
      },

      -- You can configure some alternate styling when the mouse pointer
      -- moves over inactive tabs
      inactive_tab_hover = {
        bg_color = '#3b3052',
        fg_color = '#909090',
        italic = true,

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `inactive_tab_hover`.
      },

      -- The new tab button that let you create new tabs
      new_tab = {
        bg_color = '#7aa2f7',
        fg_color = '#1f2335',

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `new_tab`.
      },

      -- You can configure some alternate styling when the mouse pointer
      -- moves over the new tab button
      new_tab_hover = {
        bg_color = '#7aa2f7',
        fg_color = '#1f2335',
        italic = true,

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `new_tab_hover`.
      },
    },
  },


  --Scrollback
  -- Enable the scrollbar.
  -- It will occupy the right window padding space.
  -- If right padding is set to 0 then it will be increased
  -- to a single cell width
  enable_scroll_bar = false,
  --How many lines of scrollback you want to retain per tab
  scrollback_lines = 3500,
  
  -- What to set the TERM variable to
  term = "xterm-256color",
  --term = "xterm-kitty",
  
  --Keys Bindings
  keys = {
    -- Copy to clipboard
    {
      key = 'C',
      mods = 'SHIFT|CTRL',
      action = wezterm.action.CopyTo 'ClipboardAndPrimarySelection',
    },

    -- Scroll by Page
    { 
      key = 'PageUp', 
      mods = 'SHIFT', 
      action = act.ScrollByPage(-0.5),
    },
    
    {
      key = 'PageDown',
      mods = 'SHIFT', 
      action = act.ScrollByPage(0.5),
    },
    -- This will create a new split  (left)
    {
      key = 'h',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.SplitPane {
        direction = 'Left',
        size = { Percent = 50 },
      },
    },

    -- This will create a new split (right)
    {
      key = 'l',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.SplitHorizontal {
        domain = 'CurrentPaneDomain',
      },
    },

    -- This will create a new split (bottom) and run your default program inside it
    {
      key = 'j',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.SplitVertical {
        domain = 'CurrentPaneDomain',
      },
    },

    -- This  will create a new split (Up)
    {
      key = 'k',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.SplitPane {
        direction = 'Up',
        size = { Percent = 50 },
      },
    },

    -- Increased Font Size
    {
      key = '+',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.IncreaseFontSize
    },

    -- Decreased Font Size
    {
      key = '-',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.DecreaseFontSize
    },

    -- Reset Font Size
    {
      key = '0',
      mods = 'CTRL',
      action = wezterm.action.ResetFontSize
    },

    -- Close Current Pane
    -- Reference:https://wezfurlong.org/wezterm/config/lua/keyassignment/CloseCurrentPane.html
    {
      key = 'w',
      mods = 'CTRL',
      action = wezterm.action.CloseCurrentPane { confirm = true },
    },

    -- Create a new tab in the same domain as the current pane.
    -- This is usually what you want.
    {
      key = 'v',
      mods = 'SHIFT|ALT',
      action = act.SpawnTab 'CurrentPaneDomain',
    },

    -- Create a new tab in the default domain
    { key = 't', mods = 'SHIFT|ALT', action = act.SpawnTab 'DefaultDomain' },

    -- Activate Pane by index
    -- Reference: https://wezfurlong.org/wezterm/config/lua/keyassignment/ActivatePaneByIndex.html
    { key = '1', mods = 'ALT', action = act.ActivatePaneByIndex(0) },
    { key = '2', mods = 'ALT', action = act.ActivatePaneByIndex(1) },
    { key = '3', mods = 'ALT', action = act.ActivatePaneByIndex(2) },
    { key = '4', mods = 'ALT', action = act.ActivatePaneByIndex(3) },
    { key = '5', mods = 'ALT', action = act.ActivatePaneByIndex(4) },
    { key = '6', mods = 'ALT', action = act.ActivatePaneByIndex(5) },
    { key = '7', mods = 'ALT', action = act.ActivatePaneByIndex(6) },
    { key = '8', mods = 'ALT', action = act.ActivatePaneByIndex(7) },
    { key = '9', mods = 'ALT', action = act.ActivatePaneByIndex(8) },
    { key = '0', mods = 'ALT', action = act.ActivateTabRelative(-1) },
    

    -- Switch Focus to Active Pane
    { key = 'LeftArrow',  mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Next',},
    { key = 'RightArrow', mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Prev',},


  },

  -- Cursor Settings
  -- The default is `SteadyBlock`.
  -- Acceptable values are `SteadyBlock`, `BlinkingBlock`,
  -- `SteadyUnderline`, `BlinkingUnderline`, `SteadyBar`,
  -- and `BlinkingBar`.
  default_cursor_style = "BlinkingBlock",
  cursor_blink_rate = 500,
  cursor_blink_ease_in = "Linear",
  --cursor_blink_ease_out = "Linear",

  -- Appearance
  window_background_opacity = 0.9, -- [1.0] alpha channel value with floating point numbers in the range 0.0 (meaning completely translucent/transparent) through to 1.0 (meaning completely opaque)
  -- Tab bar
  enable_tab_bar = false,
    -- Hide tab bar
  hide_tab_bar_if_only_one_tab = false,
  -- Disable Fancy Bar
  use_fancy_tab_bar = false,
  -- Placed tab bar at the bottom of the terminal
  tab_bar_at_bottom = true,
  -- Hide index number on tab values are : true , false
  -- Reference : https://wezfurlong.org/wezterm/config/lua/config/show_tab_index_in_tab_bar.html
  show_tab_index_in_tab_bar = false,
  -- Set this to "NeverPrompt" if you don't like confirming closing
  -- windows every time
  window_close_confirmation = "AlwaysPrompt",

}


