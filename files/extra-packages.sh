#!/bin/bash
set -xe

sudo dnf install \
    android-tools \
    arptables \
    bc \
    bcrypt \
    binutils \
    bzip2 \
    cmake \
    colordiff \
    cowsay \
    dnf-utils \
    efivar \
    elfutils \
    evince \
    figlet \
    gcc \
    gdb \
    geteltorito \
    gimp \
    git \
    glibc-devel \
    glibc-headers \
    audacious \
    gnupg \
    gnupg2 \
    gnupg2-smime \
    gnuplot \
    gparted \
    htop \
    jq \
    kernel-devel \
    kernel-headers \
    kernel-modules-extra \
    #libreoffice \
    #libyubikey \
    #libyubikey-devel \
    lsb \
    lsof \
    ltrace \
    make \
    microcode_ctl \
    most \
    ngrep \
    nmap \
    nmap-ncat \
    ntfs-3g \
    openconnect \
    openvpn \
    p7zip \
    pcsc-tools \
    perl-Image-ExifTool \
    poppler-utils \
    psmisc \
    pwgen \
    pylint \
    python \
    python-devel \
    python3-devel \ 
    python-pip \
    python-virtualenv \
    qemu-img \
    rdesktop \
    remotely \
    rpm-build \
    rpmdevtools \
    rsync \
    ruby \
    ruby-devel \
    setools \
    setools-python3 \
    setroubleshoot \
    setroubleshoot-plugins \
    setroubleshoot-server \
    ShellCheck \
    shotwell \
    sqlite \
    strace \
    sysstat \
    tar \
    telnet \
    tmux \
    tree \
    usbutils \
    uuid \
    valgrind \
    wget \
    wireshark \
    words \
    xar \
    yubikey-manager \
    yubikey-personalization-gui \
    zbar \
    qrencode

# Set RPM FUSION FREE AND NON-FREE 
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 	

sudo dnf install -y smplayer smplayer-themes 
pip install ueberzug
# The following are also good, but come from rpmfusion, install it if you want them
# https://rpmfusion.org/Configuration (be sure to verify GPG signatures)
# exfat-utils
# fuse-exfat
# unrar
# wireshark-gtk
