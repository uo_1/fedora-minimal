#!/bin/bash

# Check if Script is Run as Root
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user to run this script, please run sudo ./install.sh" 2>&1
  exit 1
fi

# Updating System
dnf update -y

# Making .config and Moving dotfiles and Background to Wallpapers 
mkdir ~/.config  
mkdir ~/Wallpapers 
chown $(whoami): ~/.config
chown $(whoami): ~/Wallpapers
mv ./files/dotconfig/*  ~/.config
mv ./files/Images/*   ~/Wallpapers 


# Creating a Screenshot Folder 
mkdir ~/Screenshot-Ordner 
chown $(whoami): ~/Screenshot-Ordner


# Installing X server and drivers
dnf -y install \
    glx-utils \
    mesa-dri-drivers \
    mesa-vulkan-drivers \
    plymouth-system-theme \
    xorg-x11-drv-evdev \
    xorg-x11-drv-fbdev \
    xorg-x11-drv-intel \
    xorg-x11-drv-libinput \
    xorg-x11-drv-vesa \
    xorg-x11-server-Xorg \
    xorg-x11-xauth \
    xorg-x11-xinit \
    xbacklight \
    redshift 


# Installing Essential Programs 
dnf install -y  bspwm sxhkd kitty rofi picom thunar nitrogen mate-polkit gnome-keyring brightnessctl playerctl  xsel xclip xkill  xsetroot tree dmenu feh unzip flameshot fzf calcurse yad ranger lightdm lightdm-gtk-greeter light-locker lightdm-gtk-greeter-settings  lightdm-settings  lightdm-gtk notification-daemon libappindicator  linux-firmware acpi rust  cargo yubibomb  cairo glib2 stalonetray    
# Installing Other Programs
echo "Installing Other Programs"
sleep 5s
dnf install mangohud
dnf install gimp golang vim neovim  lxappearance aria2 zathura zathura-pdf-poppler  gparted gnome-disk-utility  udiskie  upower xdg-utils xdg-user-dirs  xdg-user-dirs-gtk  xdg-desktop-portal dbus-x11    dnf-utils pesign keepassxc  rsync rclone  galculator viewnior  
sleep 10s 
# Installing Other Programs Printing support 
echo "Installing Other Programs Printing support"
sleep 10s
dnf install cups hplip
# Installing Other Programs 
echo "Installing openssh software"
dnf install x11-ssh-askpass openssh-askpass 
# Installing Custom ocs-url package
dnf install ./files/rpm-packages/ocs-url-3.1.0-1.fc20.x86_64.rpm
# Installing Custom gotop_v4 package  
dnf install ./files/rpm-packages/gotop_v4.2.0_linux_amd64.rpm

# Installing Network tools 
dnf install NetworkManager-tui NetworkManager-wifi network-manager-applet wireless-tools net-tools nmap  traceroute 

# Installing Sound Software 
dnf install gnome-sound-recorder pulseaudio pipewire  pipewire-doc  pipewire-gstreamer  pipewire-alsa  alsa-utils wireplumber gstreamer1 gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free-gtk gstreamer1-plugins-base gstreamer1-plugins-good pavucontrol gstreamer1-plugin-openh264 

# Installing fonts
dnf install fontawesome-fonts fontawesome-fonts-web  dejavu-sans-fonts  dejavu-sans-mono-fonts dejavu-serif-fonts adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts liberation-mono-fonts     liberation-sans-fonts   liberation-serif-fonts

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
unzip FiraCode.zip -d /usr/share/fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip
aria2c -x 4 -s 4 https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FantasqueSan     sMono.zip 
unzip Meslo.zip -d /usr/share/fonts
unzip FantasqueSansMono.zip  -d /usr/share/fonts 
# Optional Fonts 
echo "Installing optional fonts"
aria2c -x 4 -s 4 https://www.1001fonts.com/download/alte-schwabacher.zip 
unzip alte-schwabacher.zip  -d /usr/share/fonts 
aria2c -Z  https://www.1001fonts.com/download/breitkopf-fraktur.zip  https://www.1001fonts.com/download/barloesius-schrift.zip  https://www.1001fonts.com/download/strassburg-fraktur.zip https://www.1001fonts.com/download/allura.zip https://www.1001fonts.com/download/shannia.zip  
unzip '*.zip' -d /usr/share/fonts  

aria2c -x 4 -s 4 https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/JetBrainsMono.zip  
aria2c -x 4 -s 4 https://github.com/rsms/inter/releases/download/v3.19/Inter-3.19.zip   
#aria2c -x 4 -s 4 https://github.com/tabler/tabler-icons/releases/download/v1.101.0/tabler-icons-1.101.0.zip  

unzip '*.zip' -d /usr/share/fonts 

# Installing Themes 
dnf install adwaita-gtk2-theme adwaita-icon-theme  papirus-icon-theme materia-gtk-theme 
aria2c -x 4 -s 4 https://github.com/dracula/gtk/archive/master.zip 
unzip gtk-master.zip -d  /usr/share/themes/

## Source : https://github.com/ful1e5/Bibata_Extra_Cursor  
dnf install libX11-devel libXcursor-devel libpng-devel libXtst-devel  
aria2c -Z https://github.com/ful1e5/Bibata_Extra_Cursor/releases/download/v1.0.1/Bibata-Modern-DarkRed.tar.gz  https://github.com/ful1e5/Bibata_Extra_Cursor/releases/download/v1.0.1/Bibata-Modern-DodgerBlue.tar.gz   https://github.com/ful1e5/Bibata_Extra_Cursor/releases/download/v1.0.1/Bibata-Modern-Pink.tar.gz   https://github.com/ful1e5/Bibata_Extra_Cursor/releases/download/v1.0.1/Bibata-Modern-Turquoise.tar.gz   
tar -xvf BibataExtra.tar.gz   
mv Bibata-* /usr/share/icons/   

# Configure the Cursor Themes 
tar -xvf /usr/share/icons/Bibata-Modern-DarkRed.tar.gz    -C  /usr/share/icons/ 
tar -xvf /usr/share/icons/Bibata-Modern-DodgerBlue.tar.gz -C  /usr/share/icons/ 
tar -xvf /usr/share/icons/Bibata-Modern-Pink.tar.gz       -C  /usr/share/icons/ 
tar -xvf /usr/share/icons/Bibata-Modern-Turquoise.tar.gz  -C  /usr/share/icons/ 


# Reloading Font
fc-cache -vf

# Removing zip Files
rm ./FiraCode.zip ./Meslo.zip ./alte-schwabacher.zip  ./allura.zip  ./barloesius-schrift.zip  ./breitkopf-fraktur.zip  ./shannia.zip  ./strassburg-fraktur.zip  ./FantasqueSansMono.zip 

# Removing tar.gz file 
rm ./BibataExtra.tar.gz  

# Disable Openssh configure after system post 
systemctl disable sshd | true 

#  Configure Lightdm Configuration 
echo "Configure Lightdm Configuration"
sed -i 's/#user-session=default/user-session=bspwm/' /etc/lightdm/lightdm.conf 
sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-gtk-greeter/' /etc/lightdm/lightdm.conf 

# Remove tar.gz files 
rm ./usr/share/icons/Bibata-Modern-Turquoise.tar.gz ./usr/share/icons/Bibata-Modern-DarkRed.tar.gz ./usr/share/icons/Bibata-Modern-Pink.tar.gz  ./usr/share/icons/Bibata-Modern-DodgerBlue.tar.gz 

# Enabling Services and Graphical User Interface
systemctl enable lightdm.service 
systemctl set-default graphical.target

# Boot into the new environment
echo 'Rebooting in 30 seconds...'
sleep 30
sync
reboot
