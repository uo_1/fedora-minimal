#! /usr/bin/sh 
#
#Configure Lightdm 
echo "Settings up Lightdm Config File"
sed -i 's/#user-session=default/user-session=bspwm/' /etc/lightdm/lightdm.conf
sed -i 's/#greeter-session=example-gtk-gnome/greeter-session=lightdm-gtk-greeter/'  /etc/lightdm/lightdm.conf
sed -i 's/#theme-name=/theme-name=Materia-dark/'    /etc/lightdm/lightdm-gtk-greeter.conf 
sed -i 's/#icon-theme-name=/icon-theme-name=Papirus-Dark/'   /etc/lightdm/lightdm-gtk-greeter.conf
sed -i 's/#font-name=/font-name=FiraCode NF Bold 16/'    /etc/lightdm/lightdm-gtk-greeter.conf
sed -i 's/#clock-format=/clock-format=%a, %T/'    /etc/lightdm/lightdm-gtk-greeter.conf 
sed -i 's/#indicators=/indicators= ~host;~spacer;~clock;~spacer;~layout;~language;~session;~a11y;~power/'  /etc/lightdm/lightdm-gtk-greeter.conf
sed -i  '/icon-theme-name=/ a\#cursor-theme-name=Qogir-white-cursors\n#cursor-theme-size=32' /etc/lightdm/lightdm-gtk-greeter.conf
sed -i  '/screensaver-timeout=/ a\#default-user-image = /usr/share/Gesicht/'   /etc/lightdm/lightdm-gtk-greeter.conf
echo "Configuration for Lightdm Has been Completed"
