# BSPWM V-2.0

---

<table>
  <tbody>
    <tr>
      <th align="center" width="70%">Screenshot</th>
      <th align="center" width="40%">Information(Dependency)</th>
    </tr>
    <tr>
      <td>
        <img src="/files/asset/BSPWM-Catppuccin-Rice-v2.png" align="center" width="500">
        <img src="/files/asset/BSPWM-Everblush-Rice.png" align="center" width="500">
      </td>
      <td>
        <ul>
          <li><strong>Gtk Theme</strong>: <a href="https://www.pling.com/p/1365372/">kripton</a></li>
          <li><strong>Cursor</strong>: <a href="https://www.pling.com/p/1366182/">Qogir cursors</a></li>
          <li><strong>Icons Theme</strong>: <a href="https://www.opendesktop.org/p/1891042/">Zafiro Nord Dark</a></li>
          <li><strong>Fonts</strong>:
            <ul>
              <li><a href="https://github.com/rsms/inter">Inter</a>, select variable version for crisp render</li>
              <li><a href="https://github.com/JetBrains/JetBrainsMono">Jetbrains Mono</a> with nerd font patch</li>
              <li><a href="https://fontawesome.com/download">Font Awesome</a>, Icons font</li>
               <li><a href="https://github.com/tabler/tabler-icons">tabler-icons</a>, Icons font</li>
              <li><a href="https://dtinth.github.io/comic-mono-font/">Comic Mono</a>, Date & time widget</li>
              <li><a href="https://www.nerdfonts.com/">Iosevka Nerd Font</a></li>
            </ul>
          </li>
          <li><strong>Terminal</strong>: <a href="https://github.com/kovidgoyal/kitty">kitty</a> with color scheme <a href="https://github.com/folke/tokyonight.nvim/blob/main/extras/kitty/tokyonight_night.conf">tokyo night</a></li>
          <li><strong>Launcher</strong>: <a href="https://github.com/davatorium/rofi">rofi</a></li>
            <li><strong>Bar/panel</strong>: <a href="https://github.com/elkowar/eww">eww</a></li>
            <li><strong>Editor</strong>: <a href="https://github.com/neovim/neovim">neovim</a></li>
            <li><strong>Music visualizer</strong>: <a href="https://github.com/karlstav/cava">cava</a></li>
            <li><strong>Sound</strong>:
                <ul>
                    <li><a href="https://github.com/alsa-project/alsa-utils">alsa utils</a></li>
                    <li><a href="https://github.com/GeorgeFilipkin/pulsemixer">pulsemixer</a></li>
                </ul>
          <li><strong>System tray</strong>: <a href="https://github.com/kolbusa/stalonetray/">stalonetray</a></li>
          <li><strong>Notification</strong>: <a href="https://github.com/Toqozz/wired-notify">wired-notify</a></li>
          <li><strong>Color Picker</strong>: <a href="https://github.com/thezbyg/gpick">gpick</a></li>
          <li><strong>System monitor</strong>: <a href="https://github.com/xxxserxxx/gotop">gotop</a></li>
          <li><strong>Compositor</strong>: <a href="https://github.com/yshui/picom">picom</a></li>
          <li><strong>ColorScheme Generator</strong>:
            <ul>
                <li><a href="https://github.com/dylanaraps/pywal">pywal</a></li>
                <li><a href="https://github.com/deviantfero/wpgtk">wpgtk</a></li>
            </ul>
          <li><strong>Other Essential Software</strong>:
            <ul>
                <li><a href="https://github.com/derat/xsettingsd">xsettingsd</a></li>
                <li><a href="https://github.com/baskerville/xdo">xdo</a></li>
                <li><a href="https://github.com/jordansissel/xdotool">xdotool</a></li>
            </ul>
          <li><strong>Wallpapers</strong>:
          <!-- <ul> -->
          <a
          href="/files/wallpaper.md">wallpaper.md</a></li>
          <!-- <a
          href="https://wallhaven.cc/w/nkxr6q">https://wallhaven.cc/w/nkxr6q</a></li> -->
          <!-- </ul> -->
        </ul>
      </td>
    </tr>

  </tbody>
</table>

## **Video Preview**

![](/files/video/2022-12-29_18-42-25_FinalVideo.mp4)

## **Notice**

Installer Script in a stable stage now (Clean up most errors. **Untested:** Debian).

**Installer Script** : `bspwm_install.py`

Install a minimal version of the flavour/distribution that you are currently installing for the Operating System.
This is a good base environment to build your system.
Ensure you installed the necessary xorg dependency for your Operating System.

## **Installation**

<details>
    <summary><strong>Arch Linux</strong></summary>
    
```
sudo pacman -S bspwm sxhkd kitty picom rofi thunar arandr nitrogen mate-polkit gnome-keyring brightnessctl playerctl 
xsel xclip  xorg-xkill tree feh zip unzip flameshot fzf  calcurse ranger  lightdm lightdm-gtk-greeter accountsservice lightdm-gtk-greeter-settings light-locker
notification-daemon libappindicator-gtk3  acpi cairo glib2 stalonetray  gimp go vim neovim lxappearance aria2 upower 
zathura zathura-pdf-mupdf  zathura-djvu gparted gnome-disk-utility  udiskie upower neofetch xdg-utils xdg-user-dirs xdg-user-dirs-gtk  xdg-desktop-portal
keepassxc rsync rclone  galculator viewnior  openssh  x11-ssh-askpass xorg-xauth  networkmanager network-manager-applet nm-connection-editor traceroute 
networkmanager-openvpn  networkmanager-vpnc  netstat-nat  gnome-sound-recorder  pavucontrol pulseaudio  pipewire  pipewire-alsa ttf-font-awesome ttf-dejavu 
adobe-source-han-sans-cn-fonts  adobe-source-han-serif-cn-fonts  ttf-liberation  noto-fonts  gnu-free-fonts  ttf-droid  papirus-icon-theme  materia-gtk-theme
gpick  screenkey  gvfs gvfs-nfs
```
    
    
</details>

<details>
    <summary><strong>Fedora Linux</strong></summary>
    
```
sudo dnf install bspwm  sxhkd  kitty  rofi  picom  thunar  nitrogen  mate-polkit  gnome-keyring  brightnessctl  playerctl  xsel  xclip 
arandr  xkill  xsetroot  tree  feh  unzip flameshot fzf cava  calcurse ranger lightdm lightdm-gtk-greeter  lightdm-settings
lightdm-gtk-greeter-settings   light-locker notification-daemon xdotool  libappindicator linux-firmware acpi  cairo glib2 stalonetray
gimp golang  vim neovim  neofetch lxappearance  aria2  zathura  zathura-pdf-poppler gparted gnome-disk-utility
udiskie upower xdg-utils xdg-user-dirs  xdg-user-dirs-gtk xdg-desktop-portal  dbus-x11 dnf-utils  pesign  keepassxc
NetworkManager-tui  NetworkManager-wifi  network-manager-applet  net-tools  wireless-tools  nmap traceroute  
gnome-sound-recorder  pulseaudio  pipewire  pipewire-doc  pipewire-gstreamer  pipewire-alsa  alsa-utils  wireplumber
gstreamer1 gstreamer1-plugins-bad-free  gstreamer1-plugins-bad-free-gtk gstreamer1-plugins-base  gstreamer1-plugins-good pavucontrol
gstreamer1-plugin-openh264  fontawesome-fonts  fontawesome-fonts-web dejavu-sans-fonts  dejavu-sans-mono-fonts  dejavu-serif-fonts
google-droid-fonts  adobe-source-han-sans-cn-fonts  adobe-source-han-serif-cn-fonts  liberation-mono-fonts  liberation-sans-fonts  liberation-serif-fonts
adwaita-gtk2-theme  adwaita-icon-theme  papirus-icon-theme  materia-gtk-theme  libX11-devel  libXcursor-devel
libpng-devel libXtst-devel  gpick  screenkey
```
    
    
</details>

<details>
    <summary><strong>Debian Linux Untested</strong></summary>
    
```
sudo apt-get install bspwm sxhkd kitty  rofi picom thunar nitrogen mate-polkit  gnome-keyring brightnessctl
brightnessctl xsel  xclip x11-utils  x11-xserver-utils tree feh unzip arandr  flameshot fzf calcurse cava
ranger lightdm lightdm-gtk-greeter  lightdm-gtk-greeter-settings light-locker  lightdm-settings notification-daemon
acpi acpi-call-dkms stalonetray  gimp golang  vim neovim  neofetch  lxappearance aria2 zathura zathura-djvu zathura-pdf-poppler
gparted gnome-disk-utility udiskie  upower xdg-utils xdg-user-dirs xdg-user-dirs-gtk  xdg-desktop-portal
dbus-x11  keepassxc  rsync rclone galculator viewnior  openssh-client openssh-server  network-manager  nmap traceroute
pavucontrol playerctl  pipewire sound-theme-freedesktop alsa-utils pulseaudio  gnome-sound-recorder  fonts-dejavu-core  fonts-font-awesome  
fonts-liberation fonts-terminus fonts-droid-fallback fonts-noto-core  fonts-noto-mono fonts-ubuntu ttf-ubuntu-font-family

sudo apt-get install ttf-mscorefonts-installer

sudo apt-get install python3 python-is-python3 python-pip python3-xlib python3-wheel python3-apt python3-cairo
python3-dbus python3-dev python3-minimal python3-distutils python3-debian materia-gtk-theme papirus-icon-theme gpick screenkey

```


</details>

<details>
    <summary><strong>Opensuse Tumbleweed</strong></summary>

```

sudo zypper install bspwm sxhkd kitty rofi thunar picom nitrogen arandr mate-polkit gnome-keyring brightnessctl playerctl
unzip feh tree flameshot psmisc xset xsettingsd xsetpointer xsetmode xclip xsel cava libnotify-tools xkill fzf calcurse
ranger lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings light-locker notification-daemon
acpi acpid stalonetray gimp golang-org-x-sys golang-packaging vim neovim neofetch lxappearance aria2 libXss-devel xscreensaver
zathura zathura-plugin-djvu zathura-plugin-pdf-mupdf upower udiskie xdg-user-dirs xdg-user-dirs-gtk xdg-desktop-portal xdg-desktop-portal-gtk
keepassxc dbus-1-x11 gparted gnome-disk-utility rsync rclone galculator viewnior openssh openssh-server
openssh-clients nmap traceroute pavucontrol playerctl pipewire pipewire-pulseaudio sound-theme-freedesktop alsa-utils
pulseaudio gnome-sound-recorder google-droid-fonts dejavu-fonts noto-sans-fonts gnu-free-fonts liberation-fonts adobe-sourcehansans-fonts adobe-sourcehanserif-fonts
fontawesome-fonts materia-gtk-theme papirus-icon-theme gpick screenkey

```

</details>

<details>
   <summary><strong>Other Independent Linux Distributions</strong></summary>

```

The Linux philosophy is ‘Laugh in the face of danger’.
Oops. Wrong One. ‘Do it yourself’.

- Linus Torvalds

```

   </details>

**Dependency**

- git
- python >= 3.10

Clone Dotfiles and Install.
Execute the python code.

```

git clone -b BSPWM_v-2.0 https://gitlab.com/uo_1/fedora-minimal.git

cd fedora-minimal

python3 bspwm_install.py

```

## **Credit and Acknownledgement**

Inspiration for this design mainly comes from a reddit post I saw online. Credit given to this
user for time and effort to design eww widget. I used this person bar as my base and customize it.

Reddit Post:

- https://www.reddit.com/r/unixporn/comments/xw1gea/bspwm_tokyo_night_cozy_set_up/

- [rxyhn](https://github.com/rxyhn/tokyo)

- [owl4ce](https://github.com/owl4ce/dotfiles)

- [adi1090x](https://github.com/adi1090x/rofi) for the `rofi themes`

- Credits given to [AlphaTechnolog](https://github.com/AlphaTechnolog/void-bspwm) for the `getWinTitle` script
```

